def main():
    while True:
        code = input("1. Шифрование \n 2. Дешифрование \n")
        if (code == "1"):
            encrypt()
        elif (code == "2"):
            decrypt()
        else:
            break

def decrypt():
    key = input("Введите ключевое слово \n")
    text = input("Введите текст \n")

    if (len(text) % len(key) == 0):
        count_groups = (len(text) / len(key))
    else:
        count_groups = len(text) / len(key) + 1
    count_groups = int(count_groups)
    #количество зануляемых групп
    count_zero = count_groups * len(key) - len(text)
    # заполнение массива с порядком столбцов

    tmp_key = key
    order_mass = []
    for i in range(len(key)):
        min = 10000000
        index = -1
        for j in range(len(tmp_key)):
            if (ord(tmp_key[j]) < min):
                min = ord(tmp_key[j])
                index = j
        order_mass.append(index)

        tmp_key = tmp_key.replace(tmp_key[index], chr(2000),1)

    #проходит по группам, если номер группы один из последних, зануляет
    for i in range(len(key)):
        if(order_mass[i]>max(order_mass)-count_zero):
            text = text[:count_groups*i+count_groups-1] + "🤡" + text[count_groups*i+count_groups-1:]

    # создает матрицу из зануленных значений
    x = [["🤡" for i in range(len(key))] for j in range(int(count_groups))]

    ch_index = 0
    for index in order_mass:
        for row in x:
            row[index] = text[ch_index]
            ch_index += 1

    decoding_text = ''
    for row in x:
        for col in row:
            decoding_text = decoding_text + col
    decoding_text = decoding_text.replace("🤡", '')
    print(decoding_text)

def encrypt():
    key = input("Введите ключевое слово \n")
    text = input("Введите текст \n")
    #text = text.replace(" ", "")
    mass = [] #массив с группированными буквами
    #высота массива
    if (len(text) % len(key) == 0):
        height = (len(text) / len(key))
    else:
        height = len(text) / len(key) + 1

    count = 0
    #заполнение массива с буквами
    for i in range(int(height)):
        tmp_mass = []
        for j in range(len(key)):
            if (count == len(text)):
                break
            else:
                tmp_mass.append(text[count])
                count += 1
        mass.append(tmp_mass)


    # заполнение массива с порядком столбцов
    tmp_key = key
    order_mass = []
    for i in range(len(key)):
        min = 10000000
        index = -1
        for j in range(len(tmp_key)):
            if (ord(tmp_key[j]) < min):
                min = ord(tmp_key[j])
                index = j
        order_mass.append(index)
        tmp_key = tmp_key.replace(tmp_key[index], chr(2000),1)
    final_string = ""
    #создание зашифрованной строки
    for i in order_mass:
        for j in mass:
            if (len(j) > i):
                final_string = final_string + j[i]
    print(final_string)

if __name__ == '__main__':
    main()




